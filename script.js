'use strict'

// МАССИВ ИЗ ЭЛЕМЕНТОВ, ПОЛУЧЕННЫХ ПО КЛАССУ 'calcButton'
const buttons = Array.from(document.getElementsByClassName('calcButton'));

// ПРИСВАИВАЕМ ПЕРЕМЕННОЙ ПО АЙДИ 'input'
const input = document.getElementById('input');

function calculate() {
    const expression = input.value; // ПРИСВАИВАЕМ ПЕРЕМЕННОЙ ЗНАЧЕНИЕ 'input.value'
    const numbers = expression.split(/[-+*\/]/) // МАССИВ ЦИФР
    const operations = expression.split(/\d+/g) // МАССИВ ОПЕРАЦИЙ
    let result = Number(numbers[0]);
    for (let i = 0; i < numbers.length; i++) {
        const op = operations[i + 1];
        let num2 = Number(numbers[i + 1])
        if (isNaN(num2)) {
            num2 = 0;
        }

        switch (op) {
            case '+':
                result = result + num2;
                break;
            case '-':
                result = result - num2;
                break;
            case '.':
                result = result + num2;
                break;
            case '*':
                break;
            case '/':
                break;
        }
    }
    input.value = result;
    console.log(result)
}


// УНИВЕРСАЛЬНЫЙ ОБРАБОТЧИК КЛИКА ПО КНОПКЕ КАЛЬКУЛЯТОРА
function handelClick(event) {
    const currentButton = event.target; // ПРИСВАИВЕМ ПЕРЕМЕННОЙ ЭЛЕМЕНТ НА КОТОРОМ СРАБОТАЛ ОБРАБОТЧИК
    const symbol = currentButton.innerHTML; // ПРИСВАИВАЕМ ПЕРЕМЕННОЙ СОДЕРЖИМОЕ НАЖАТОЙ КНОПКИ
    const lastSymbol = input.value.slice(-1); // ПРИСВАИВАЕМ ПЕРЕМЕННОЙ ПОСЛЕДНЕИЙ СИМВОЛ input

    // УСЛОВИЕ ДЛЯ ОЧИСТКИ input
    if (symbol === 'C') // СРАВНИВАЕМ СИМВОЛ КНОПКИ С 'С'
    {
        input.value = ''; // ОЧИЩАЕМ ЗНАЧЕНИЕ input (ВСТАВЛЯЕМ ПУСТУЮ СТРОКУ)
        return; // ОСТАНАВЛИВАЕМ ДАЛЬНЕЙШЕЕ ВЫПОЛНЕНИЕ ФУНКЦИИ
    }

    if (symbol === '&lt;') {
        // ПРИСВАИВАЕМ ЗНАЧЕНИЮ input ПРЕДЫДУЩЕЕ ЗНАЧЕНИЕ БЕЗ ПОСЛЕДНЕГО СИМВОЛА
        input.value = input.value.slice(0, -1);
        return
    }

    if (symbol === '+' && lastSymbol === '+') {
        // alert('Can not use + twice, dumbass')
        return;
    }

    if (symbol === '-' && lastSymbol === '-') {
        return
    }

    if (symbol === '*' && lastSymbol === '*') {
        return
    }

    if (symbol === '/' && lastSymbol === '/') {
        return
    }

    if (symbol === '.' && lastSymbol === '.') {
        return
    }

    if (symbol === '=') {
        calculate();
        return;
    }

    input.value = input.value + symbol; // ДОБАВЛЯЕМ ТЕКУЩЕМУ ЗНАЧЕНИЮ input СИМВОЛ
    input.focus(); // ПЕРЕВЕДЕНИЕ ФОКУСА НА input
}

// ВЫЗЫВАЕМ ФУНКЦИЮ ДЛЯ КАЖДОГО ЭЛЕМЕНТА МАССИВА КНОПОК
buttons.forEach(function(element) {
    // ДОБАВЛЯЕМ ОБРАБОТЧИК НАЖАТИЯ НА КНОПКУ
    element.addEventListener('click', handelClick);
});
















// // ADD ONE
// const buttonOne = document.getElementById('item-1');

// buttonOne.addEventListener("click", addOne);

// function addOne() { input.value = input.value + 1; }

// // ADD TWO

// const buttonTwo = document.getElementById('item-2');

// buttonTwo.addEventListener("click", addTwo);

// function addTwo() { input.value = input.value + 2; }

// // ADD THREE

// const buttonThree = document.getElementById('item-3');

// buttonThree.addEventListener("click", addThree);

// function addThree() { input.value = input.value + 3; }

// // ADD FOUR

// const buttonFour = document.getElementById('item-4');

// buttonFour.addEventListener("click", addFour);

// function addFour() { input.value = input.value + 4; }

// // ADD FIVE

// const buttonFive = document.getElementById('item-5');

// buttonFive.addEventListener("click", addFive);

// function addFive() { input.value = input.value + 5; }

// // ADD SIX

// const buttonSix = document.getElementById('item-6');

// buttonSix.addEventListener("click", addSix);

// function addSix() { input.value = input.value + 6; }

// // ADD SEVEN

// const buttonSeven = document.getElementById('item-7');

// buttonSeven.addEventListener("click", addSeven);

// function addSeven() { input.value = input.value + 7; }

// // ADD EIGHT

// const buttonEight = document.getElementById('item-8');

// buttonEight.addEventListener("click", addEight);

// function addEight() { input.value = input.value + 8; }

// // ADD NINE

// const buttonNine = document.getElementById('item-9');

// buttonNine.addEventListener("click", addNine);

// function addNine() { input.value = input.value + 9; }

// // ADD ZERO

// const buttonZero = document.getElementById('item-0');

// buttonZero.addEventListener("click", addZero);

// function addZero() { input.value = input.value + 0; }

// // ADD PLUS

// const buttonPlus = document.getElementById('item-plus');

// buttonPlus.addEventListener("click", addPlus);

// function addPlus() { input.value = input.value + '+'; }

// // function addPlus() {
// //     input.value = input.value + '+';
// //     if (input.value == '+') {
// //         alert('Can not use + twice')
// //         console.log('nixuya')
// //     } else
// //         input.value + "+"
// //     console.log('plus')

// // }

// // ADD MINUS

// const buttonMinus = document.getElementById('item-minus');

// buttonMinus.addEventListener("click", addMinus);

// function addMinus() { input.value = input.value + '-'; }

// // ADD MULTIPLICATION

// const buttonX = document.getElementById('item-x');

// buttonX.addEventListener("click", addX);

// function addX() { input.value = input.value + 'x'; }

// // ADD DEVISION

// const buttonDevision = document.getElementById('item-slash');

// buttonDevision.addEventListener("click", addDevision);

// function addDevision() { input.value = input.value + '/'; }

// // ADD DOT

// const buttonDot = document.getElementById('item-dot');

// buttonDot.addEventListener("click", addDot);

// function addDot() { input.value = input.value + '.'; }